> Tell us some stories about people in your community using maps?
> Who are they?
> What are they doing when they use the map?
> What do they expect? (or not expect)

# Terrastories user stories

[Terrastories](terrastories.app), by design, is a relatively simple application which can be used to map any stories or media content with a connection to a place. Hence, the users of Terrastories range from Indigenous communities who wish to create a repository of knowledge of place-based stories shared by elders, to scholars who are mapping historical sites, to diaspora communities who are using to illustrate the journeys that their parents or grandparents took to arrive from their homelands to the place they live today. 

However, our co-creation partners have always been Indigenous or other local communities, and it is their user stories which we most closely follow when building new features of Terrastories.

By default:
- All data in Terrastories is restricted to a community (data includes Stories, Speakers, and Places).
- A Place can have multiple Stories and a Story can have multiple Speakers. Users can upload any kind of media for Stories. Places also have a feature to add placename pronunciation and a photo.
- Multiple communities can use on Terrastories server. Currently, the data is stored in a Postgres db.
- A Terrastories community is accessible through a login, which has a role (currently: Viewer, Member, Editor, Admin, we want to make this more granular).
- Each Terrastories community can have their own map, which can either be an online map (e.g. Mapbox.com) or an offline map package (`style.json` with one or more raster or vector `mbtiles`, fonts, and glyphs).
- Maps are not just on the main application; we are also using maps throughout the CMS to show locations and permit users to add their Places without needing lat/longs.
- A new feature for online users called "Explore Terrastories" allows communities to opt-in to sharing a selection of their Stories with a wider public (who do not need to log in) on a default Mapbox map.

General requirements for Terrastories:
- must be free
- must be translatable
- must be easy to use and fun to play with
- all aspects of the application must work offline

Tech stack
- Ruby on Rails (CMS, authentication, package management)
- React (front end)
- Postgres (database)
- Mapbox / MapLibre (mapping library)
- Tileserver-GL (offline map server)
- Docker (deployment)

Hosting:
- Online (Heroku, DigitalOcean, etc.)
- Offline "Field Kit" generating a WiFi hotspot
- Mesh Network
- Kakawa / Earth Defenders Toolkit

It is worth noting that for most of the life of Terrastories, it has been a volunteer-built operation, benefiting from early partnership with Ruby for Good, taking advantage of built-a-thon events like Hacktoberfest, and the kindness and interest of several open-source developers and maintainers. This is part of the origin story of Terrastories that makes it unique and gives us inspiration; but it has also meant that we've had to deal with tech debt of insufficiently informed architecture decisions, lesser quality code contributions from junior contributors, and major delays on commonly requested features that are not as "fun" to work on. We only got dedicated funding twice, and relatively small sums to only build out limited features. As of this year, Dd is providing some small support for maintenance and hosting as well. In spite of that, it's amazing to me (Rudo) how far we've come, with a relatively bug-free and functional app that by now far exceeds our initial MVP goals! 

## 1. More than just a place name: documenting richly detailed place-based knowledge using the map

The origin story of Terrastories, and the most common entry point to wanting to use Terrastories, starts with an Indigenous community who is doing territory mapping work, and realizes that they are only documenting the tip of the iceberg by gathering the geospatial location and the name of a place. 

To paint a picture: a group of community members is on a mapping expedition on a river. They are in the process of gathering data for making a map for some purposes (e.g. to demonstrate ancestral occupation of a region, for the eventual purpose of land rights). There are young people with GPS or a smartphone mapping app, and an elder who has a lifetime of experience traveling up and down the river. They come across a hill in the middle of the river. The elder signals for the boat to stop, and mentions that this is an important place worth having on the map. He shares the name of the map, which the youth note down in their devices. But he goes on to share a detailed story about what happened before at this place, which relates to the early history of this community, shared with him by his elders before him. In the process, he sings a song, and uses material objects nearby like a banana leaf to paint a vivid picture for the youth. They marvel and delight in the story. Yet, the extent of the knowledge that was documented was limited to lat/long coordinates, and the place name. And insofar as there are now roads leading to this community, most of the young people will never experience this story again when the current generation of elders passes on.

One of the primary objectives of Terrastories is to allow communities to add this story to their map. They can record the elder sharing the story in audio or video form, and link it to the place with relative ease (i.e. no technical knowledge required).

## 2. Interactive web maps, but offline-first: how to get the community maps working?

When we started working on Terrastories in 2017, the communities wanting to use it were entirely offline in remote villages in the Amazon, and often complained that the scarce resources available about their culture were only accessible over the internet. Hence, we decided to make Terrastories offline-first - all features of the application must work offline, entirely.

This was relatively straightforward for most of the application, except for the maps. There are libraries and tools like [Tileserver-GL](https://github.com/maptiler/tileserver-gl), but getting custom maps working for communities was, and remains, tricky. And custom maps is often a requirement: publicly available maps like OpenStreetMap often do not reflect the worldview of Indigenous communities, as their local knowledge or lived experience is simply not represented on these maps at all.

For communities that are doing mapping projects (or being supported by an Ally NGO), they are often working with spatial data in a few common formats - `Shapefile`, `KML/KMZ`, or `GPX`, and using either QGIS or ArcGIS to style the data. To get this in a format that can be used by the offline-compatible mapping libraries (usually `mbtiles` or `xyz`), and to then generate a `style.json` files which styles the data, is not easy, and has a demanding learning curve attached to it. (The best way is to upload your data to Mapbox and to use Mapbox Studio, which can be described as an Adobe Illustrator-esque environment for styling maps.)

The very first build of Terrastories tried in part to solve this problem by bundling `tippecanoe` to automatically convert `shapefiles` into `mbtiles` when starting the application. But this still required uploading the same data to Mapbox Studio and downloading the `style.json` from there, but this was not at all intuitive and required some manual steps to get it all working.

The Mapeo team built a `mapbox-style-downloader` Node package that can scrape `XYZ` tiles from Mapbox Studio together with the `style.json` and other assets, but at the time of writing this still requires a step to convert the `XYZ` tiles into `mbtiles`. (That will likely change as Mapeo is now switching to using `mbtiles` for their map server). And it is still a command line operation.

There is also the issue that Mapbox no longer has an open-source license, and requires entering a credit card to use their Freemium services. So how can users style their maps? The only alternative currently is [Maputnik](https://github.com/maputnik/editor), which is starting to be supported again; but even this user interface might be too much for our users.

In short, this absence of a workflow to generate offline maps is a major detriment to the autonomy of community users, and it is not unique to Terrastories - I imagine it applies for all of the applications in **p2p-maps** wishing to provide maps offline.

## 3. Sharing stories across local instances of Terrastories

From the very beginning, our users asked a question: if we have a local build of Terrastories in Pusugrunu (a village), and we have a local build of Terrastories in Nyun Jakobkondre (another instance), and we add some stories in Pusugrunu, how do we get those to appear in Nyun Jakobkondre?

More recently, other users have been using our online servers for staging their build of Terrastories, and then want to use that to deploy an offline instance. How to do that?

The answer right now is that everything has to be done manually. These are not problems we have been able to solve (largely due to its complexity, and not having the bandwidth of volunteers willing to work on it). But our vision is to eventually use a p2p protocol instead of a centralized db. The Terrastories dev team does not have experience working with p2p and have not scoped out what this would entail, or how many resources it would take. But it's a long-term aspiration. It makes sense for a lot of reasons.

## 4. Using Terrastories with other applications

Many of our users are not using Terrastories in isolation. Some are using Mapeo to collect the spatial data. Others are building [story maps](https://github.com/digidem/maplibre-storymap) to tell a story using maps for advocacy or visibility. Others are interested in using Ahau. Others have a database mapping data, only part of which they want to visualize as place-based stories, and other parts are for monitoring or data visualization.  Some even have been exposed to Kakawa (or Earth Defenders Toolkit offline) where Terrastories is presented as one application for the task of **Land-based storytelling**.

Given this user story, we are interested in increasing the degree of interoperability between other applications. This is definitely a UX challenge, but even before getting into that, there is the question of shared assets. One of our users asks: "how do I get my background map in Terrastories working easily in Mapeo, in Ahau, and in our other web mapping tools"? Currently, this requires a technical person to know how to convert map tiles from one expected format to the other, and to place the files in the right location. Ideally, this should be a whole lot easier.
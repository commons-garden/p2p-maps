> Tell us some stories about people in your community using maps?
> Who are they?
> What are they doing when they use the map?
> What do they expect? (or not expect)

# Mapeo user stories

By default:

- all data lives on a local device (currently working on support for cloud-based syncing)
- data syncing is enabled with anyone sharing the same "project key"

General requirements for Mapeo:

- must be free
- must be translatable
- must be easy to use and fun to play with
- all aspects of the application must work offline
- prioritize the usage of open source software

Tech stack

- React + React Native
- Mapbox
- NodeJS
- Hypercore
- SQLite

## 1. Geographically-linked data collection (observations)

## 2. Map Editing

## 3. Map Customization for Offline Usage

> Tell us some stories about people in your community using maps?
> Who are they?
> What are they doing when they use the map?
> What do they expect? (or not expect)


# LibreMesh user stories

[LibreMesh](https://libremesh.org/) is an operating system for wireless mesh networks.

Users of LibreMesh are usually laypeople in small to medium size rural villages from the global south that create and maintain their own telecommunications infrastructure for their own use and that of their neighbours within their territory.

In general users use the app every now and then, not regularly. Intensively during deployments but then once per month tops to do maintenance, sometimes less.

Users are laypeople, neighbours, non-techincally trained people.

Users interact with LibreMesh when:
- they deploy mesh nodes, by planning the node location, checking where their neighbours have their nodes, aligning the antennas to point to their neighbours
- they maintain the links between mesh nodes, might happen regularly or after a storm or strong winds, that requires to adjust antenna allignment.
- to implement governance decisions, like opening or password protecting the access to the network or implementing a voucher system for free riders.

The LibreMesh co-creation community has been conformed of technicians and users of the network that are close to the programmers.

Maps are used within LibreMesh for locating the locations where mesh nodes are fixed, to visualize the wireless links between these nodes, to have a spatial sense of the overlay between the network and the community.

Maps are loaded from OpenStreetMaps, so they only work when there is a path to the internet.

It would be desirable to have a local cache of the basemap of the community, and also encourage mapping within the community beyond the network infrastructure, as the richer the map gets, the more valuable the overlay will be for network mapping purposes.

Location information together with the rest of the node config is stored in the mesh nodes and synced in a peer to peer way to their neighbours in a flood-like approach.

Location information doesn't leave the community.

Users interact with the map from a mobile app, the LiMe-app

## Tech stack

- a Progressive Web App written in Preact that interacts directly with the wireless node.
- The backend on the wireless node is written in Lua. 


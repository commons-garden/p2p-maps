> Tell us some stories about people in your community using maps?
> Who are they?
> What are they doing when they use the map?
> What do they expect? (or not expect)

# Ahau user stories

Our user stories are all about community members using the application [Ahau](ahau.io).
By default:
- all data in Ahau is encrypted to that group (a.k.a. tribe)
- all groups have subgroup(s) which allow more fine-grained access control

General requirements for Ahau:
- must be free
- must work on mobile
    - we currently run NodeJS inside Cordova for mobile

## 1. Coordinating around legally defined land blocks

The maori land court split the land into "blocks".
To legally work with the land Families have to coordinate "the land owners" around those blocks to make decisions together.
Many whanau have pooled their ownership into a "land trust" to coordinate the management.
So within a land-trust some decisions may involve only some families.

"Landowners interests" are generally determined by your share based on the number children/generations of the original ancestor named on the landblock.

Personas: 
- a general family member
- a living landowner
- the coordinator of a land trust 

What they want:
- map a particular land block
- name it
- who are the people association
  - A) who is the living landowner
  - B) a geneology
  - C) a list of registered as owners since the land was split
- we would like to be able to group them
  - "I want to see all the pieces of land related to our family"
- we want to be able to identify land blocks which have no living landowner

These land blocks can change over time:
- can be joined
- can be split

One of the largest challenges is some blocks have land-owners who cannot be found.
If actions are taken, and a decendant turns up ... we have a mess to deal with.
There are ~$20 million in unpaid dividends...


## 2. Linking place + stories

e.g. we discribe a historical event, and would like to show where that was.

Persona: a member of an Ahau tribe

What they want
- when I create a story I would like to be able to add a:
  - location
  - area
  - path
- I would like to be able to look at a map, and see which stories are in an area
- I expect these details to be private to our tribe/family

- may not need to be fully offline


## 3. Linking place + living people

We would love to see where our (living) people are on a map.
It would be helpful to have statistics about:
- how many people are on our traditional land
- how many people are overseas
- etc.

We might like to be able to drop in on a cuzzy (cousin) if we're in some town.

For some people it is going to be important to have a registered postal address.
- addresses can be validated by an API (in NZ)



## 4. Linking place + sites of significance

e.g.
- burial site
- fishing place
- last place a bird was sighted
- location of different plants
  - for traditional costumes, etc.

expected to be private to a group

Note this is similar but different to stories. Concept not built in Ahau yet


